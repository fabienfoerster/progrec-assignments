package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }
  
  
  property("gen1") = forAll { (h: H) =>
  	val m = if (isEmpty(h)) 0 else findMin(h)
  	findMin(insert(m, h))==m
  }
  
  property("min2") = forAll { (a: Int, b: Int) =>
  	val h = insert(b,empty)
  	val h2 = insert(a,h)
  	findMin(h) == a || findMin(h) == b
  }
  
  property("empty") = forAll { a: Int =>
  	val h = insert(a,empty)
  	deleteMin(h) == empty
  }
  
  property("meld") = forAll { (h1: H , h2: H) =>
  	val h = meld(h1,h2)
  	findMin(h) == findMin(h1) || findMin(h) == findMin(h2)
  }
  
  property("meld2") = forAll { (h1: H , h2: H) =>
  	val h3 = meld(h1,h2)
  	val h4 = insert(findMin(h1),h2)
  	val h5 = deleteMin(h1)
  	val h6 = meld(h4,h5)
  	heapToSeq(h3) == heapToSeq(h6)
  }
  
  property("ord") = forAll { h: H => 
  	isOrdered(heapToSeq(h))
  }

  val heapToSeq: H => List[Int] = (h: H) => 
    if(isEmpty(h)) Nil
    else findMin(h) :: heapToSeq(deleteMin(h))
  
  val isOrdered : List[Int] => Boolean = (l: List[Int]) =>
    if(l.isEmpty) true
    else 
      if(l.tail forall (_ >= l.head)) isOrdered(l.tail)
      else false
    
  lazy val genHeap: Gen[H] = for {
    k <- arbitrary[Int]
    m <- oneOf(empty,genHeap)
  } yield insert(k,m)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

}
